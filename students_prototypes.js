var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

/* 1. Создать конструктор Student для объекта студента, который принимает имя и кол-во баллов в качестве аргумента. Все объекты созданные этим конструктором должны иметь общий метод show, который работает так же как в предыдущем домашнем задании. */

function Student (name, point) {
	this.name = name;
	this.point = point;
};
Student.prototype.show = function () {
	console.log('Студент %s набрал %s баллов', this.name, this.point);
};

/* 2. Создать конструктор StudentList, который в качестве аргумента принимает название группы и массив формата studentsAndPoints из задания по массивам. 
Массив может отсутствовать или быть пустым. 
Объект, созданный этим конструктором должен обладать всеми функциями массива. 
А так же иметь метод add, который принимает в качестве аргументов имя и кол-во баллов, создает нового студента и добавляет его в список. */

function StudentList (group, array) {
	this.group = group;	
	this.list = [];
	if (typeof array != 'undefined') {
		for (var i = 0; i < array.length; i += 2) {
			this.list.push(new Student (
				array[i],
				array[i+1]
			));
		}
	}    
	this.add = function (name, point) {
		this.list.push(new Student (
			name,
			point
		));
	};
};

/* 3. Создать список студентов группы «HJ-2» и сохранить его в переменной hj2. Воспользуйтесь массивом studentsAndPoints. */

var hj2 = new StudentList('HJ-2', studentsAndPoints);

/* 4. Добавить несколько новых студентов в группу. Имена и баллы придумайте самостоятельно. */

hj2.add('Иванов Иван', 20);
hj2.add('Петров Иван', 20);

/* 5. Создать группу «HTML-7» без студентов и сохранить его в переменной html7. После чего добавить в группу несколько студентов. Имена и баллы придумайте самостоятельно. */

var html7 = new StudentList('HTML-7');
html7.add('Иванов Петр', 30);
html7.add('Петров Петр', 30);

/* 6. Добавить спискам студентов метод show, который выводит информацию о группе в следующем виде:
Группа HTML-7 (2 студентов): 
Студент Наталья Терентьева набрал 10 баллов
Студент Кирилл Васильев набрал 30 баллов
Вывести информацию о группах «HJ-2» и «HTML-7». */

StudentList.prototype.show = function () {
	console.log('Группа %s (%s студентов):', this.group, this.list.length);
	this.list.forEach(function (value) {
		value.show();
	});
};

hj2.show();
html7.show();

/* 7. Перевести одного из студентов из группы «HJ-2» в группу «HTML-7» */

StudentList.prototype.translation = function (name, group) {
	var indexStudent = this.list.findIndex(function (value, i) {
		return value.name == name;
  });
  if (indexStudent > -1) {
  	var tempArray = this.list.splice(indexStudent, 1);
		group.add(tempArray[0].name, tempArray[0].point);
  }  
};

hj2.translation('Ирина Овчинникова', html7);