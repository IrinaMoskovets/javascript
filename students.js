var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

/* 1. Вывести список студентов */
console.log('Список студентов:');
for (var i = 0, imax = studentsAndPoints.length - 1; i < imax; i += 2) {
	console.log('Студент ' + studentsAndPoints[i] + ' набрал ' + studentsAndPoints[i + 1] + ' баллов') 
}

console.log('');

/* 2. Найти студента набравшего наибольшее количество баллов, и вывести информацию о нем */
var maxIndex = -1, max, nameStudent; 
for (var i = 0, imax = studentsAndPoints.length - 1; i < imax; i += 2) {
	if (maxIndex < 0 || studentsAndPoints[i + 1] > max) {
		maxIndex = i + 1;
		max = studentsAndPoints[i + 1]; 
		nameStudent = studentsAndPoints[i];
	} 
} 
console.log('Студент набравший максимальный балл:');
console.log('Студент ' + nameStudent + ' имеет максимальный бал ' + max);

console.log('');

/* 3. Добавить новых студентов */
studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);

/* 4. Студенты «Антон Павлович» и «Николай Фролов» набрали еще по 10 баллов, внести изменения в массив */
var studentsIndex = studentsAndPoints.indexOf('Антон Павлович');
if (studentsIndex != -1) {
	studentsAndPoints[studentsIndex + 1] += 10;
}
studentsIndex = studentsAndPoints.indexOf('Николай Фролов');
if (studentsIndex != -1) {
	studentsAndPoints[studentsIndex + 1] += 10;
}

/* 5. Вывести список студентов, не набравших баллов */
console.log('Студенты не набравшие баллов:');
for (var i = 0, imax = studentsAndPoints.length - 1; i < imax; i += 2) {
	if (studentsAndPoints[i + 1] == 0) {
		console.log(studentsAndPoints[i]);
	} 
} 

console.log('');

/* 6. Удалить из массива данные о студентах, набравших 0 баллов */
/*for (var i = 0, imax = studentsAndPoints.length - 1; i < imax; i += 2) {
	if (studentsAndPoints[i + 1] == 0) {
		studentsAndPoints.splice(i, 2);
		i -= 2;
	} 
}*/
if (studentsAndPoints.indexOf(0) !== -1) {
	for (var i = studentsAndPoints.length-1; i >= 0; i -= 2) {
		if (studentsAndPoints[i] == 0) {
			studentsAndPoints.splice(i-1, 2);
		} 
	}
}