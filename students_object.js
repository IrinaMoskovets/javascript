var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

/* 1. Создать массив students. Заполнить его данными из studentsAndPoints. Каждый элемент массива это объект в поле name имя студента а в поле point его балл. Так же добавить метод show который выводит в консоль строку вида: Студент Виктория Заровская набрал 30 баллов. */
var students = [],
	show = function () {
		console.log('Студент %s набрал %s баллов', this.name, this.point);
	};

for (var i = 0; i < studentsAndPoints.length; i += 2) {
	students.push({
		name: studentsAndPoints[i],
		point: studentsAndPoints[i+1],
		show: show
	});
}

/* 2. Добавить в список студентов «Николай Фролов» и «Олег Боровой». У них пока по 0 быллов.*/
students.push({
	name: 'Николай Фролов',
	point: 0,
	show: show
});

students.push({
	name: 'Олег Боровой',
	point: 0,
	show: show
});

/* 3. Увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30, а Николаю Фролову на 10. */
function incPoints(name, point) {
	var findStudent = students.find(function (value) {
    return value.name == name;
  });
  if (typeof findStudent !== 'undefined') {
  	findStudent.point += point;
  }
};

incPoints('Ирина Овчинникова', 30);
incPoints('Александр Малов', 30);
incPoints('Николай Фролов', 10);

/* 4. Вывести список студентов набравших 30 и более баллов без использования циклов. */
console.log('Список студентов:');
students.forEach(function (value, i) {
	if (value.point >= 30) {
		value.show();
	}
});

/* 5. Учитывая что каждая сделанная работа оценивается в 10 баллов, добавить всем студентам поле worksAmount равное кол-ву сделанных работ.*/
students.forEach(function (value, i) {
	value.worksAmount = value.point / 10;
});


/* Дополнительное задание: добавить объекту students метод findByName, который принимает на вход имя студента и возвращает соответствующий объект, либо undefined. */
students.findByName = function (nameStudent) {
	return students.find(function (value, i) {
		return value.name == nameStudent;
	});
};

console.log(students.findByName('Александр Малов'));