var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

/* 1. Создать два массива students и points и заполнить их данными из studentsAndPoints без использования циклов. В первом массиве фамилии студентов, во втором баллы. Индексы совпадают. */
var students = studentsAndPoints.filter(function (value) {
	return (typeof value === 'string');
});
var points = studentsAndPoints.filter(function (value) {
	return (typeof value === 'number');
});

/* 2. Вывести список студентов без использования циклов */
console.log('Список студентов:');
students.forEach(function (value, i) {
	return console.log('Студент ' + value + ' набрал ' + points[i] + ' баллов');
});

console.log('');

/* 3. Найти студента набравшего наибольшее количество баллов */
var max, maxIndex;
points.forEach(function (number, i) {
	if (!max || number > max) {
		max = number;
		maxIndex = i;
	}
});
console.log('Студент набравший максимальный балл: ' + students[maxIndex] + ' (' + max + ' баллов)');

/* 4. Используя метод массива map увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30 */
points = points.map(function (value, i) {
	if (students[i] == 'Ирина Овчинникова' || students[i] == 'Александр Малов') {
  	return value + 30;
  } else {
  	return value;
  }
});

console.log('');

/* 5. Дополнительное задание: реализовать функцию getTop, которое принимает на вход число и возвращает массив в формате studentsAndPoints, в котором соответствующее кол-во студентов с лучшими баллами в порядке убывания кол-ва баллов. Используя эту функцию вывести в консоль топ студентов */

var getTop = function (top){
	var students2 = students.slice(),
		points2 = points.slice(),
		tempArray = [];
	for (i = 0; i < top; i++) {	
		max = 0;
		maxIndex = 0;
		points2.forEach(function (number, i) {
			if (!max || number > max) {
				max = number;
				maxIndex = i;    
			}
		});
		tempArray = tempArray.concat(students2.splice(maxIndex, 1), points2.splice(maxIndex, 1)); 
	}
	return tempArray;
};

var top = 3,
	studentsAndPoints2 = getTop(top);

console.log('Топ ' + top + ':');  
for (i = 0; i < studentsAndPoints2.length; i += 2) {
	console.log(studentsAndPoints2[i] + ' - ' + studentsAndPoints2[i+1] + ' баллов');
}